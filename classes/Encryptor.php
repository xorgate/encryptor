<?php

namespace PWS\Utils;

class Encryptor {
	private $_privateKeyLocation   = null;
	private $_privateKeyPassphrase = null;
	private $_publicKeyLocation    = null;
	private $_privateKey 		   = null;
	private $_publicKey 		   = null;
	
	public function __construct($publicKeyLocation, $privateKeyLocation = null , $privateKeyPassphrase = null) {
		$this->_privateKeyLocation   = $privateKeyLocation;
		$this->_privateKeyPassphrase = $privateKeyPassphrase;
		$this->_publicKeyLocation    = $publicKeyLocation;
		
		if (!is_null($privateKeyLocation))
			$this->_privateKey = openssl_pkey_get_private(file_get_contents($privateKeyLocation), $privateKeyPassphrase ? $privateKeyPassphrase : null);
		if (!is_null($publicKeyLocation))
			$this->_publicKey  = openssl_pkey_get_public(file_get_contents($publicKeyLocation));
	}
	
    public function decrypt($data) {
		if (openssl_private_decrypt(base64_decode($data), $decryptedData, $this->_privateKey))
			return $decryptedData;
		return false;
    }

    public function encrypt($data) {
		if(openssl_public_encrypt($data, $encryptedData, $this->_publicKey))
			return base64_encode($encryptedData);
		return false;
    }
}