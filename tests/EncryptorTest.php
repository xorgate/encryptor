<?php

use PWS\Utils\Encryptor;

class EncryptorTest extends PHPUnit_Framework_TestCase {
	
	public function testCanBeInstanciated() {
		$tmpFile = sys_get_temp_dir() . '/tmp-' . md5(microtime());
		file_put_contents($tmpFile, 'a');
		
		$encryptor = new Encryptor($tmpFile);
		$this->assertTrue(is_object($encryptor));
	}
	
	public function testCanEncrypt() {
		$keypair = $this->generateKeyPair();
		
		$encryptor = new Encryptor($keypair['public'], $keypair['private']);
		$encryptedValue = $encryptor->encrypt('test');
		
		
		$this->assertTrue(is_string($encryptedValue));
	}
	
	public function testCanEncryptAndDecrypt() {
		
	}
	
	private function generateKeyPair() {
		$privateKeyOpts = openssl_pkey_new([
			'private_key_bits' => 2048,
			'private_key_type' => OPENSSL_KEYTYPE_RSA
		]);
		openssl_pkey_export($privateKeyOpts, $privateKey);
		$privateKeyDetails = openssl_pkey_get_details($privateKeyOpts);		
		$pubKey = $privateKeyDetails['key'];
		openssl_pkey_free($privateKeyOpts);
		
		return [
			'public' => $pubKey,
			'private' => $privateKey
		];
	}
}